const express = require("express");
const Formation = require("./../models/formation");

const router = express.Router();

router.get("/new", (req, res) => {
  res.render("formations/new", { formation: new Formation() });
});

router.get("/:slug", async (req, res) => {
  const formation = await Formation.findOne({ slug: req.params.slug });
  if (formation == null) res.redirect("/");
  res.render("formations/show", { formation: formation });
});

router.get("/edit/:id", async (req, res) => {
  const formation = await Formation.findById(req.params.id);
  res.render("formations/edit", { formation: formation });
});

router.post("/", async (req, res, next) => {
  req.formation = new Formation();
  next();
}, saveFormationsAndRed('new'));

router.put("/:id", async (req, res, next) => {
  req.formation = await Formation.findById(req.params.id);
  next();
}, saveFormationsAndRed('edit'));

router.delete("/:id", async (req, res) => {
  await Formation.findByIdAndDelete(req.params.id);
  res.redirect("/");
});

// Middleware

function saveFormationsAndRed(path) {
  return async (req, res) => {
    let formation = req.formation;
    formation.title = req.body.title;
    formation.description = req.body.description;
    formation.markdown = req.body.markdown;
    formation.location = req.body.location;
    formation.centerTime = req.body.centerTime;
    formation.companyTime = req.body.companyTime;
    formation.validationType = req.body.validationType;
    formation.actionType = req.body.actionType;
    formation.center_id = req.body.centerId;

    try {
      formation = await formation.save();
      res.redirect(`/formations/${formation.slug}`);
    } catch (e) {
      res.render(`formations/${path}`, { formation: formation });
    }
  };
}

module.exports = router;
