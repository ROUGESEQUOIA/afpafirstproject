const express = require("express"); // Accès aux fonctionnalités du framework Express.js
const mongoose = require("mongoose"); // Accès à la bdd non relationnelle
const methodOverride = require('method-override'); // permet d’ajouter des methods aux formulaires (delete, patch, put)

// Import des modèles

const Formation = require("./models/formation");

// Import des routes

const formationsRouter = require("./routes/formations"); // routes des formations voir bas de script pour l’use

//

mongoose.connect("mongodb://localhost/afpaformapp", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  /* useCreateIndex: true, */
});

// Config

const app = express(); // Utilisation d’Express par l’app

app.set("view engine", "ejs"); // permet de convertir les views ejs en html
app.use(express.urlencoded({ extended: false })); // permet de récupérer les req.body
app.use(methodOverride('_method')); // permet d’utiliser les nouvelles methods

// Route par défaut

app.get("/", async (req, res) => {
  const formations = await Formation.find().sort({ createdAt: "desc" });
  res.render("formations/index", { formations: formations });
});

//  Utilisation des routes

app.use("/formations", formationsRouter); // permettre à l’app d’utiliser le routeur et lui donner l’instruction concernant l’url

//

app.listen(5000);
