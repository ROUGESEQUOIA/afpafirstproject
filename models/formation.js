const mongoose = require("mongoose"); // import du package permettant la connexion à la bdd
const { marked } = require("marked"); // convertisseur markdown
marked.use(
  {mangle: false,
  headerIds: false,}
); // Options dépréciée
const slugify = require("slugify");
const createDomPurify = require('dompurify'); // Permet de purifier l’input markdown pour empêcher l’injection de script
const { JSDOM } = require('jsdom'); // Permet à node d’interpréter du HTML
const dompurify = createDomPurify(new JSDOM().window); // permet de créer du HTML via JSDOM et de le purifier avec la méthode createDomPurify

const formationSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  markdown: {
    type: String,
    required: true,
  },
  location: {
    type: String,
  },
  centerTime: {
    type: Number,
  },
  companyTime: {
    type: Number,
  },
  validationType: {
    type: String,
  },
  actionType: {
    type: String,
  },
  center_id: {
    type: Number,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  slug: {
    type: String,
    required: true,
    unique: true,
  },
  sanitizedHTML: {
    type: String,
    required: true,
  },
});

formationSchema.pre("validate", function(next) { // fonction lancée à chaque interrogation de la bdd
  if (this.title) {
    this.slug = slugify(this.title, {
      lower: true,
      strict: true, // permet de supprimer les caractères spéciaux
    });
  
  if (this.markdown) {
    this.sanitizedHTML = dompurify.sanitize(marked.parse(this.markdown))
  }
  
    next();
  }
});

module.exports = mongoose.model("Formation", formationSchema);
